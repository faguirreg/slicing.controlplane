import networkx as nx
import SDN_REST.links as Links
import SDN_REST.nodes as Nodes
import SDN_REST.stats as Stats
import SDN_REST.datapaths as Datapaths
import metrics_module as metrics
import time
from threading import Thread
from copy import deepcopy

# Parameters
sleep_time = 1
# Data
links = Links.list_links()
port_capacity = {}
stats = []
# Graphs
net_connectivity = None
net_bandwidth = None

def monitor_time(t):
    time_ = sleep_time - (time.time() - t)
    if time_ < 0:
        return 0
    return time_

def calc_connectivity():
    global links, net_connectivity
    while True:
        t = time.time()
        G = nx.Graph()
        links = Links.list_links()
        for l in links:
            if (l['dst_dpid'], l['src_dpid']) not in G.edges:
                G.add_edge(l['src_dpid'], l['dst_dpid'], object=l)
        nodes = Nodes.all_nodes()
        for n in nodes:
            G.add_edge(n['ip'], n['dpid'], object=n)
        net_connectivity = G
        #print(dict(G.nodes))
        # print()
        # print(dict(G.edges))
        # print()
        time.sleep(monitor_time(t))

def push_port_stats():
    t = time.time()
    while True:
        time.sleep(monitor_time(t))
        stats = Stats.all_port_stats()
        if not stats:
            print('WARNING: Could not get stats.')
            continue
        for dpid, port_stats in stats.items():
            for stat in port_stats:
                if stat['port_id'] <= 48:
                    rx_point = {
                        'measurement': 'PortRxStats',
                        'tags': {'datapath': dpid, 'port_no': stat['port_id']},
                        'fields': {
                            'rx_packets': stat['rx_packets'], 'rx_bytes': stat['rx_bytes'],
                            'rx_dropped': stat['rx_dropped'], 'rx_errors': stat['rx_errors']
                            }
                        }
                    tx_point = {
                        'measurement': 'PortTxStats',
                        'tags': {'datapath': dpid, 'port_no': stat['port_id']},
                        'fields': {
                            'tx_packets': stat['tx_packets'], 'tx_bytes': stat['tx_bytes'],
                            'tx_dropped': stat['tx_dropped'], 'tx_errors': stat['tx_errors']
                            }
                        }
                    col_point = {
                        'measurement': 'PortCollisions',
                        'tags': {'datapath': dpid, 'port_no': stat['port_id']},
                        'fields': {
                            'collisions': stat['collisions']}
                        }
                    metrics.db_handler.write_points([rx_point, tx_point, col_point])
        t = time.time()

def push_port_utilization(src_dpid, src_port, dst_dpid, dst_port, used, available):
    src_point = {
        'measurement': 'PortUtilization',
        'tags': {'datapath': src_dpid, 'port_no': src_port},
        'fields': {'used': used, 'available': available}
        }
    dst_point = {
        'measurement': 'PortUtilization',
        'tags': {'datapath': dst_dpid, 'port_no': dst_port},
        'fields': {'used': used, 'available': available}
        }
    metrics.db_handler.write_points([src_point, dst_point])

def get_link_capacity(src_dpid, src_port, dst_dpid, dst_port):
    src_speed = port_capacity[src_dpid][src_port]
    dst_speed = port_capacity[dst_dpid][dst_port]
    return min(src_speed, dst_speed) # in kbps

def get_link_utilization(dpid, port):
    now, prev = 0, 0
    global stats
    t = 2
    if len(stats) > 1:
        if len(stats) == 2:
            t = 1
        for s in stats[0][dpid]:
            if s['port_id'] == port:
                rx_bytes = s['rx_bytes']
                tx_bytes = s['tx_bytes']
                prev = rx_bytes + tx_bytes
                break
    else:
        return 0
    for s in stats[-1][dpid]:
        if s['port_id'] == port:
            rx_bytes = s['rx_bytes']
            tx_bytes = s['tx_bytes']
            now = rx_bytes + tx_bytes
            break
    return int(((now - prev)/t)/125) # kbps (bytes per second / 125 = x kbps)

def check_port_capacity(is_thread = False):
    global port_capacity
    while True:
        t = time.time()
        for dpid in Datapaths.list_dpids():
            port_capacity.setdefault(dpid, {})
            for port_info in Datapaths.ports(dpid):
                if is_int(port_info['id']) and 'current_speed' in port_info:
                    port_id = int(port_info['id'])
                    if (port_id) not in port_capacity[dpid]:
                        port_capacity[dpid].setdefault(port_id, port_info['current_speed'])
                    else:
                        port_capacity[dpid][port_id] = port_info['current_speed']
        if not is_thread: return
        time.sleep(monitor_time(t))

def calc_bandwidth():
    global net_bandwidth, stats
    while True:
        G = nx.Graph()
        t = time.time()
        if len(stats) == 3:
            stats.pop(0)
        last_stats = Stats.all_port_stats()
        if not last_stats:
            print('WARNING: Could not get stats.')
            continue
        stats.append(last_stats)
        for l in links:
            src_dpid, dst_dpid = l['src_dpid'], l['dst_dpid']
            src_port, dst_port = l['src_port'], l['dst_port']
            if (dst_dpid, src_dpid) not in G.edges:
                link_capacity = get_link_capacity(src_dpid, src_port, dst_dpid, dst_port)
                link_utilization = get_link_utilization(src_dpid, src_port) # tx in one side is rx in the other side
                #print(f'{src_dpid} - {dst_dpid} - Utilization: {link_utilization} / {link_capacity}')
                Thread(target=push_port_utilization, args=(src_dpid, src_port, dst_dpid, dst_port, link_utilization, link_capacity)).start()
                G.add_edge(src_dpid, dst_dpid, object={'used': link_utilization, 'capacity': link_capacity})
        nodes = Nodes.all_nodes()
        for n in nodes:
            G.add_edge(n['ip'], n['dpid'])
        net_bandwidth = G
        time.sleep(monitor_time(t))

def install(path, bw, priority, slice_id=None):
    # Get bw in Mbps, converto to kbps.
    bw = bw*1000
    src_ip = path[0]
    dst_ip = path[-1]
    path = path[1:-1]
    if not slice_id:
        slice_id = 1000
    match1 = [{'ipv4_src': src_ip}, {'ipv4_dst': dst_ip},{'eth_type':'ipv4'}]
    match2 = [{'ipv4_src': dst_ip}, {'ipv4_dst': src_ip},{'eth_type':'ipv4'}]
    for i in range(len(path)):
        this_dpid = path[i]
        if i == 0:
            try:
                in_port = dict(net_connectivity.edges)[(src_ip, this_dpid)]['object']['port']
            except KeyError:
                in_port = dict(net_connectivity.edges)[(this_dpid, src_ip)]['object']['port']
        else:
            prev_dpid = path[i-1]
            try:
                details = dict(net_connectivity.edges)[(prev_dpid, this_dpid)]['object']
            except KeyError:
                details = dict(net_connectivity.edges)[(this_dpid, prev_dpid)]['object']
            if this_dpid == details['src_dpid']:
                in_port = details['src_port']
            else:
                in_port = details['dst_port']
        if i == len(path)-1:
            try:
                out_port = dict(net_connectivity.edges)[(dst_ip, this_dpid)]['object']['port']
            except KeyError:
                out_port = dict(net_connectivity.edges)[(this_dpid, dst_ip)]['object']['port']
        else:
            next_dpid = path[i+1]
            try:
                details = dict(net_connectivity.edges)[(next_dpid, this_dpid)]['object']
            except KeyError:
                details = dict(net_connectivity.edges)[(this_dpid, next_dpid)]['object']
            if this_dpid == details['src_dpid']:
                out_port = details['src_port']
            else:
                out_port = details['dst_port']
        # CREATING METERS
        meter_dict = {'version': '1.3.0', 'meter': {
                    'id': slice_id, 'command': 'add',# command: modify, id = slice_id
                    'flags': ['kbps', 'stats'], 
                    'bands': [{'burst_size': bw,'rate': int(bw*1.02),'mtype': 'drop'}]
                    }}
        Datapaths.create_meter(this_dpid, meter_dict)
        instructions1 = [{"meter":slice_id},{'apply_actions':[{'output': out_port}]}]
        instructions2 = [{"meter":slice_id},{'apply_actions':[{'output': in_port}]}]
        flow_dict1 = {'flow':
                {
                    'table_id': 100, 'priority': priority,
                    'idle_timeout': 0, 'hard_timeout': 0,
                    'match': match1, 'instructions': instructions1
                }
            }
        flow_dict2 = {'flow':
                {
                    'table_id': 100, 'priority': priority,
                    'idle_timeout': 0, 'hard_timeout': 0,
                    'match': match2, 'instructions': instructions2
                }
            }
        Datapaths.create_flow(this_dpid, flow_dict1)
        Datapaths.create_flow(this_dpid, flow_dict2)

def is_int(x):
    try:
        int(x)
        return True
    except ValueError:
        return False

check_port_capacity()
connectivity_thread = Thread(target=calc_connectivity)
stats_thread = Thread(target=push_port_stats)
bandwidth_thread = Thread(target=calc_bandwidth)
capacity_thread = Thread(target=check_port_capacity, args=(True,))
connectivity_thread.start()
stats_thread.start()
bandwidth_thread.start()
capacity_thread.start()