import urllib3, json, requests
from config import controller, controller_url, json_header
from SDN_REST.auth import logged_header

def controller_stats():
    """
    List controller statistics for all controllers that are part of this controller's team.
    """
    url = f'{controller_url}/of/stats'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        stats = result.json()["controller_stats"]
        return stats
    else:
        print(f'ERROR: {result.json()}')
        return None

def all_port_stats():
    """
    Dictionary of all port statistics of all datapaths.
    """
    url = f'{controller_url}/of/stats/ports'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        stats = {}
        for dp_ in result.json()["stats"]:
            if 'port_stats' in dp_:
                stats[dp_['dpid']] = dp_['port_stats']
        return stats
    else:
        print(f'ERROR: {result.json()}')
        return None

def port_stats(dpid, port = None):
    """
    If a port is given, list the statistics of the port for the given datapath,
    else list the statistics of all ports in that datapath.

    Parameters:
      - dpid (str): The datapath id.
      - port (int): The port id.

    Returns:
      - List of port statistics.
    """
    url = f'{controller_url}/of/stats/ports?dpid={dpid}'
    if port: url += f'&port_id={port}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        stats = result.json()["port_stats"]
        return stats
    else:
        print(f'ERROR: {result.json()}')
        return None

def all_meter_stats():
    """
    Dictionary with all meter statistics of all datapaths.
    """
    url = f'{controller_url}/of/stats/meters'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        stats = {}
        for dp_ in result.json()["stats"]:
            if 'meter_stats' in dp_:
                stats[dp_['dpid']] = dp_['meter_stats']
        return stats
    else:
        print(f'ERROR: {result.json()}')
        return None

def meter_stats(dpid, meterid = None):
    """
    If a meterid is given, list the statistics of the meter for the given datapath,
    else list the statistics of all meters in that datapath.

    Parameters:
      - dpid (str): The datapath id.
      - meterid (int): The meter id.
    Returns:
      - List of meter stats (if only dpid is given).
      - Dictionary of meter stats (if both dpid and meterid are given).
    """
    url = f'{controller_url}/of/stats/meters?dpid={dpid}'
    if meterid: url += f'&meterid={meterid}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        stats = result.json()["meter_stats"]
        return stats
    else:
        print(f'ERROR: {result.json()}')
        return None