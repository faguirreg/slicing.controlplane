from requests import post
import urllib3

urllib3.disable_warnings()

def login(controller, controller_url, json_header):
    url = f'{controller_url}/auth'
    data = f'{{"login": {{"user": "{controller["user"]}", "password": "{controller["pwd"]}", "domain": "sdn"}}}}'
    result = post(url, data=data, headers=json_header,verify=False)
    if result.status_code == 200:
        json_token = result.json()
        # print(json_token)
        return json_token['record']['token']

def logged_header(controller, controller_url, json_header):
    return {
        "Content-Type": "application/json",
        "X-Auth-Token": login(controller, controller_url, json_header)
    }