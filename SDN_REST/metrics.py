import urllib3, json, requests
from config import controller, controller_url, json_header
from SDN_REST.auth import logged_header

def apps():
    """
    Gets the names and IDs of all applications that have metrics persisted on the system.
    """
    url = f'{controller_url}/metrics/apps'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        apps = result.json()["apps"]
        return apps
    else:
        print(f'ERROR: {result.json()}')
        return None

def app_metrics(app_id):
    """
    Gets the list of all metrics for the specified parameters in the system.
    """
    url = f'{controller_url}/metrics/apps/{app_id}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        metrics = result.json()["metrics"]
        return metrics
    else:
        print(f'ERROR: {result.json()}')
        return None

def app_names(app_id):
    """
    Gets the set of all metric names for the specified parameters in the system.
    """
    url = f'{controller_url}/metrics/apps/{app_id}/names'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        names = result.json()["names"]
        return names
    else:
        print(f'ERROR: {result.json()}')
        return None

def app_primaries(app_id):
    """
    Gets the set of all primary tags for the specified parameters in the system.
    """
    url = f'{controller_url}/metrics/apps/{app_id}/primaries'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        primaries = result.json()["primaries"]
        return primaries
    else:
        print(f'ERROR: {result.json()}')
        return None

def app_secondaries(app_id):
    """
    Gets the set of all secondary tags for the specified parameters in the system.
    """
    url = f'{controller_url}/metrics/apps/{app_id}/secondaries'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        secondaries = result.json()["secondaries"]
        return secondaries
    else:
        print(f'ERROR: {result.json()}')
        return None

def metric(metric_uid):
    """
    Gets the metric with the specified metric identifier in the system.
    """
    url = f'{controller_url}/metrics/{metric_uid}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        metric = result.json()["metric"]
        return metric
    else:
        print(f'ERROR: {result.json()}')
        return None

def metric_values(metric_uid):
    """
    Gets the list of all metric values for the specified parameters in the system.
    """
    url = f'{controller_url}/metrics/{metric_uid}/values'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        metric = result.json()#["metric"]
        return metric
    else:
        print(f'ERROR: {result.json()}')
        return None