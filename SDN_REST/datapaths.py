import urllib3, json, requests
from config import controller, controller_url, json_header
from SDN_REST.auth import logged_header

def list_datapaths():
    """
    Get the list of datapaths with details.
    """
    url = f'{controller_url}/of/datapaths'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        datapaths = result.json()["datapaths"]
        return datapaths
    else:
        print(f'ERROR: {result.json()}')
        return None

def list_dpids():
    """
    Get the list of datapaths ids.
    """
    datapath_list = list_datapaths()
    if datapath_list:
        dpids = [dp['dpid'] for dp in datapath_list]
        return dpids
    else:
        return None

def datapath(dpid):
    """
    Get the details of a given datapath.

    Parameters:
      - dpid (str): The datapath id.
    Returns:
      - Dictionary with the details of the datapath.
    """
    url = f'{controller_url}/of/datapaths/{dpid}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        datapath = result.json()["datapath"]
        return datapath
    else:
        print(f'ERROR: {result.json()}')
        return None

### P O R T S
def ports(dpid):
    """
    List all ports details of a given datapath.

    Parameters:
      - dpid (str): The datapath id.
    Returns:
      - List of port details.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/ports'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        ports = result.json()["ports"]
        return ports
    else:
        print(f'ERROR: {result.json()}')
        return None

def port_ids(dpid):
    """
    List all port ids of a given datapath.

    Parameters:
      - dpid (str): The datapath id.
    Returns:
      - List of port ids.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/ports'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        ports = result.json()["ports"]
        ports = [port['id'] for port in ports]
        return ports
    else:
        print(f'ERROR: {result.json()}')
        return None

def port(dpid, port_id):
    """
    Get details of a given port of a datapath.

    Parameters:
      - dpid (str): The datapath id.
      - port_id (int): The port id.
    Returns:
      - Dictionty with port details.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/ports/{port_id}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        port = result.json()["port"]
        return port
    else:
        print(f'ERROR: {result.json()}')
        return None

def port_action(dpid, port_id, action):
    """
    Enable or disable port on a given datapath.

    Parameters:
      - dpid (str): The datapath id.
      - port_id (int): The port id.
      - action (bool): Enable/disable.
    Returns:
      - True if success.
      - False if error.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/ports/{port_id}/action'
    action = 'enable' if action else 'disable'
    result = requests.post(url, headers=logged_header(controller, controller_url, json_header), data=action, verify=False)
    if result.status_code == 202:
        return True
    else:
        print(f'ERROR: {result.json()}')
        return False

### F L O W S
def flows(dpid):
    """
    List all flows of a given datapath.

    Parameters:
      - dpid (str): The datapath id.
    Returns:
      - List of flow details (dict).
    """
    url = f'{controller_url}/of/datapaths/{dpid}/flows'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        flows = result.json()["flows"]
        return flows
    else:
        print(f'ERROR: {result.json()}')
        return None

def create_flow(dpid, flow_dict, purpose = None):
    """
    Create a flow entry for a datapath.

    Parameters:
      - dpid (str): The datapath id.
      - flow_dict (dict): The flow parameters.
      - purpose (str): Optional flow class ID which indicates this flow's purpose.
    Returns:
      - True if success.
      - False if error.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/flows'
    if purpose: f'{url}?purpose={purpose}'
    result = requests.post(url, headers=logged_header(controller, controller_url, json_header), data=json.dumps(flow_dict), verify=False)
    if result.status_code == 201:
        return True
    else:
        print(f'ERROR: {result.text}')
        return False

def update_flow(dpid, flow_dict, purpose = None):
    """
    Modify a flow entry.

    Parameters:
      - dpid (str): The datapath id.
      - flow_dict (dict): The flow parameters.
      - purpose (str): Optional flow class ID which indicates this flow's purpose.
    Returns:
      - True if success.
      - False if error.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/flows'
    if purpose: f'{url}?purpose={purpose}'
    result = requests.put(url, headers=logged_header(controller, controller_url, json_header), data=json.dumps(flow_dict), verify=False)
    if result.status_code == 204:
        return True
    else:
        print(f'ERROR: {result.json()}')
        return False

def delete_flow(dpid, flow_dict, purpose = None):
    """
    Delete a flow entry.

    Parameters:
      - dpid (str): The datapath id.
      - flow_dict (dict): The flow parameters.
      - purpose (str): Optional flow class ID which indicates this flow's purpose.
    Returns:
      - True if success.
      - False if error.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/flows'
    if purpose: f'{url}?purpose={purpose}'
    result = requests.delete(url, headers=logged_header(controller, controller_url, json_header), data=json.dumps(flow_dict), verify=False)
    if result.status_code == 204:
        return True
    else:
        print(f'ERROR: {result.json()}')
        return False

### P I P E L I N E
def pipeline(dpid = None):
    """
    If a dpid is given, gets the pipeline definition for the given datapath, else gets the custom pipeline definition used by this controller for all datapaths with a customizable pipeline.

    Parameters:
      - dpid (str): The datapath id.
    Returns:
      - Dictionary with the pipeline description.
    """
    url = f'{controller_url}/of/datapaths/pipeline'
    if dpid:
        url = f'{controller_url}/of/datapaths/{dpid}/pipeline'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        return result.json()
    else:
        print(f'ERROR: {result.json()}')
        return None

### M E T E R S
def meter_features(dpid):
    """
    List a datapath's meter features

    Parameters:
      - dpid (str): The datapath id.
    Returns:
      - List of features.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/features/meter'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        features = result.json()["meter_features"]
        return features
    else:
        print(f'ERROR: {result.json()}')
        return None


def meters(dpid):
    """
    List all meters for this datapath.

    Parameters:
      - dpid (str): The datapath id.
    Returns:
      - List of meters (dict).
    """
    url = f'{controller_url}/of/datapaths/{dpid}/meters'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        meters = result.json()["meters"]
        return meters
    else:
        print(f'ERROR: {result.json()}')
        return None

def meter(dpid, meter_id):
    """
    Get meter detail information.

    Parameters:
      - dpid (str): The datapath id.
      - meter_id (int): The meter id.
    Returns:
      - Dictionary of meter details.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/meters/{meter_id}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        meter = result.json()["meter"]
        return meter
    else:
        print(f'ERROR: {result.json()}')
        return None

def create_meter(dpid, meter_dict):
    """
    Create a new meter for the given datapath.

    Parameters:
      - dpid (str): The datapath id.
      - meter_dict (dict): The meter parameters.
    Returns:
      - True if success.
      - False if error.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/meters'
    result = requests.post(url, headers=logged_header(controller, controller_url, json_header), data=json.dumps(meter_dict), verify=False)
    if result.status_code == 201:
        return True
    else:
        print(f'ERROR: {result.json()}')
        return False

def update_meter(dpid, meter_id, meter_dict):
    """
    Modify information on a given meter on the given datapath.

    Parameters:
      - dpid (str): The datapath id.
      - meter_id (dict): The meter id.
      - meter_dict (dict): The meter parameters.
    Returns:
      - True if success.
      - False if error.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/meters/{meter_id}'
    result = requests.put(url, headers=logged_header(controller, controller_url, json_header), data=json.dumps(meter_dict), verify=False)
    if result.status_code == 200:
        return True
    else:
        print(f'ERROR: {result.json()}')
        return False

def delete_meter(dpid, meter_id):
    """
    Delete the given meter on the given datapath.

    Parameters:
      - dpid (str): The datapath id.
      - meter_id (dict): The meter id.
    Returns:
      - True if success.
      - False if error.
    """
    url = f'{controller_url}/of/datapaths/{dpid}/meters/{meter_id}'
    result = requests.delete(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 204:
        return True
    else:
        print(f'ERROR: {result.json()}')
        return False