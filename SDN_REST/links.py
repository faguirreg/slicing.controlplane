import urllib3, json, requests
from config import controller, controller_url, json_header
from SDN_REST.auth import logged_header

def list_links(dpid = None):
    """
    Get the list of links for a given datapath: If a dpid
    is not given, then list all links for all datapaths.

    Parameters:
      - dpid (str): The datapath id.
    Returns:
      - List of links.
    """
    url = f'{controller_url}/net/links'
    if dpid: url += f'?dpid={dpid}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        links = result.json()["links"]
        return links
    else:
        print(f'ERROR: {result.json()}')
        return None

def shortest_path(src_dpid, dst_dpid):
    """
    Get the set of nodes to traverse for shortest path between given source and destination (datapaths).
    """
    url = f'{controller_url}/net/paths/forward?src_dpid={src_dpid}&dst_dpid={dst_dpid}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        links = result.json()['path']['links']
        return links
    else:
        print(f'ERROR: {result.json()}')
        return None