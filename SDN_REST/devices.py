import urllib3, json, requests
from config import controller, controller_url, json_header
from SDN_REST.auth import logged_header

def list_devices():
    """
    Get the list of devices with details.
    """
    url = f'{controller_url}/net/devices'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        devices = result.json()["devices"]
        return devices
    else:
        print(f'ERROR: {result.json()}')
        return None

def device(uid):
    """
    Get the information of a specific device.
    """
    url = f'{controller_url}/net/devices/{uid}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        device = result.json()
        return device
    else:
        print(f'ERROR: {result.json()}')
        return None

def interfaces(uid):
    """
    Get the list of all interfaces of a specific device.
    """
    url = f'{controller_url}/net/devices/{uid}/interfaces'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        interfaces = result.json()['Interfaces']
        return interfaces
    else:
        print(f'ERROR: {result.json()}')
        return None