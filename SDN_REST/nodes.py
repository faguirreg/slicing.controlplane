import urllib3, json, requests
from config import controller, controller_url, json_header
from SDN_REST.auth import logged_header

def all_nodes():
    """
    Get the list of all end nodes.

    Parameters:
      - dpid (str): The datapath id.
    Returns:
      - List of nodes.
    """
    url = f'{controller_url}/net/nodes'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        nodes = result.json()["nodes"]
        return nodes
    else:
        print(f'ERROR: {result.json()}')
        return None

def nodes_by_dpid(dpid, port = None):
    """
    Get the list of learned end nodes for a given datapath ID.
    If a port is provided, it returns the list of end nodes
    in that port.

    Parameters:
      - dpid (str): The datapath id.
      - port (int): The port number.
    Returns:
      - List of nodes.
    """
    url = f'{controller_url}/net/nodes?dpid={dpid}'
    if port: url += f'&port={port}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        nodes = result.json()["nodes"]
        return nodes
    else:
        print(f'ERROR: {result.json()}')
        return None

def node_by_ip(ip, vid = None):
    """
    Get the learned end node with a given ip address.
    Optionally a VLAN ID can be included to the filter.

    Parameters:
      - ip (str): The ip address of the end node.
      - vid (int): The VLAN ID.
    Returns:
      - Node details (dict).
    """
    url = f'{controller_url}/net/nodes?ip={ip}'
    if vid: url += f'&vid={vid}'
    result = requests.get(url, headers=logged_header(controller, controller_url, json_header), verify=False)
    if result.status_code == 200:
        try:
            node = result.json()["nodes"][0]
            return node
        except:
            print('There are not nodes.')
            return None
    else:
        print(f'ERROR: {result.json()}')
        return None