from influxdb import InfluxDBClient

DB_NAME = 'MetricsRepository'

db_handler = InfluxDBClient(host='127.0.0.1', port=8086)
# db_handler.drop_database(DB_NAME)
# db_handler.create_database(DB_NAME)
db_handler.switch_database(DB_NAME)

#201.72